//Incluindo biblioteca do sensor ultrassonico
#include <Ultrasonic.h>

#define PINO_TRIGER 2 // Constante para o pino trigger do sensor ultrasonico
#define PINO_ECHO 3 // Constante para o pino echo do sensor ultrasonico
#define BUZZER 7 // Constante para o pino do buzzer
#define PINO_POT A0 // Constante para o pino do potenciômetro
#define PINO_LED 12 // Constante para o led
#define PINO_BOTAO 13 // Constante para o botão

int valorPot = 0; // Guarda o valor do potênciometro
int estadoBotao = 0; // Guarda o valor de estado do botão
 
//Inicializa o sensor ultrasonico nos pinos
Ultrasonic ultrasonic(PINO_TRIGER, PINO_ECHO);
 
void setup()
{
  Serial.println("Iniciando aplicação...");
  Serial.begin(9600);
  pinMode(BUZZER,OUTPUT);
  pinMode(PINO_BOTAO,INPUT);
  pinMode(PINO_LED,OUTPUT);
}
 
void loop()
{
    if(estadoBotao == LOW){
        estadoBotao = digitalRead(PINO_BOTAO);
    }else{
        if(digitalRead(PINO_BOTAO) == HIGH){
            estadoBotao = 0;
        }
    }
  
    if(estadoBotao == HIGH){

        digitalWrite(PINO_LED, HIGH);
        
        float distancia;
        long tempoMicroSegundos = ultrasonic.timing();
        distancia = ultrasonic.convert(tempoMicroSegundos, Ultrasonic::CM);
      
        Serial.print("Distancia em cm: ");
        Serial.println(distancia);
      
        valorPot = analogRead(PINO_POT); // ler o valor do potenciômetro
        float limiteBase = valorPot / 10; // Diminui uma casa decimal do valor do potênciometro
      
        Serial.print("Valor do limite base: ");
        Serial.print(limiteBase);
        Serial.println(" cm");
      
        if(distancia <= limiteBase){
           float limite = limiteBase - (limiteBase * 0.7); // Limite final com valor de menos 70% do limite base

            Serial.print("Valor do limite final: ");
            Serial.print(limite);
            Serial.println(" cm");

            if(distancia <= limite){
               tone(BUZZER, 1500);
               digitalWrite(PINO_LED, LOW);
               delay(200);
               digitalWrite(PINO_LED, HIGH);
            }else{
              tone(BUZZER, 1500);
              delay(500);
              noTone(BUZZER);
            }

        }else{
           noTone(BUZZER);
        }
   
    }else{
      Serial.println("Aplicação desligada!");
      digitalWrite(PINO_LED, LOW);
    }
  

  delay(1000);
}
