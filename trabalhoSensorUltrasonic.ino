//Incluindo biblioteca do sensor ultrassonico
#include <Ultrasonic.h>

#define PINO_TRIGER 2; // Constante para o pino trigger do sensor ultrasonico
#define PINO_ECHO 3; // Constante para o pino echo do sensor ultrasonico
#define PINO_POT 4; // Constante para o pino do potênciometro
#define PINO_LED_VERMELHO 5; // Constante para o pino do led vermelho
#define PINO_LED_VERDE 6; // Constante para o pino do potênciometro
#define PINO_BUZZER 7; // Constante para o pino do potênciometro

int valorPot = 0; // Para o valor do potênciometro
 
//Inicializa o sensor ultrasonico nos pinos
Ultrasonic ultrasonic(PINO_TRIGER, PINO_ECHO);
 
void setup()
{
  Serial.begin(9600);
  Serial.println("Iniciando aplicação...");
  //pinMode(ledPin, OUTPUT);  // declarar o pino ledPin como saída

  
}
 
void loop()
{
  //Le as informacoes do sensor e faz a conversão para centímetros
  float centimetros;
  long tempoMicroSegundos = ultrasonic.timing();
  centimetros = ultrasonic.convert(tempoMicroSegundos, Ultrasonic::CM);

  Serial.print("Distancia em cm: ");
  Serial.print(centimetros);

  valorPot = analogRead(pinoPot); // ler o valor do potenciômetro
  long limiteCentimetros = valorPot / 10; // Diminui uma casa decimal do valor do potênciometro
  long limiteAlertaAmarelo = limiteCentimetros / 10;
  long limiteAlertaVermelho = limiteAlertaAmarelo - 5;

  if(centimetros <= limiteAlertaAmarelo){
    digitalWrite(PINO_LED_AMARELO, HIGH);  // ligar o led amarelo
    digitalWrite(PINO_LED_VERDE, LOW);  // desligar o led verde
    digitalWrite(PINO_LED_VERMELHO, LOW);  // desligar o led vermelho

    if(){
      
    }
    
  }
  
  if(centimetros <= limitetCentimetros){
     digitalWrite(PINO_LED_VERDE, LOW);  // desligar o led verde
     digitalWrite(PINO_LED_VERMELHO, HIGH);  // ligar o led vermelho
  }else{
     digitalWrite(PINO_LED_VERMELHO, LOW); // desligar o led vermelho
     digitalWrite(PINO_LED_VERDE, HIGH);  // ligar o led verde
  }
  

  

  delay(1000);
}
